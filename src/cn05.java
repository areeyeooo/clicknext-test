import java.util.Scanner;

public class cn05 {
	public static void main(String[] args) {
		Scanner kb = new Scanner(System.in);
		int index = 0;
		int[] arr = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };

		System.out.print("Input number 1-4: ");
		int n = kb.nextInt();
		if (n >= 5) {
			System.out.println("Please input number 1-4!!!");
		} else {
			for (int i = 0; i < n; i++) {
				for (int j = n - i; j > 1; j--) {
					System.out.print(" ");
				}
				for (int j = 0; j <= i; j++) {
					System.out.print(arr[index] + " ");
					index++;
				}
				System.out.println();
			}
		}
	}
}
