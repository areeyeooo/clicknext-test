import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class cn09 {

	public static void main(String[] args) {
		Scanner kb = new Scanner(System.in);
		System.out.print("Enter sentence: ");
		String word = kb.nextLine();
		String reverted = "";
		List<String> arr = Arrays.asList(word.split(" "));
		for(int i = 0;i < arr.size();i++) {
			reverted += revert(arr.get(i)) + " ";
		}
		System.out.println(reverted);
	}
	
	public static String revert(String s) {
		char[] letters = new char[s.length()];
		
		int letterIndex = 0;
		for(int i = s.length() - 1; i >= 0; i--) {
			letters[letterIndex] = s.charAt(i);
			letterIndex++;
		}
		
		String r = "";
		for(int i = 0; i < s.length(); i++) {
			r = r + letters[i];
		}
		return r;
	}

}

