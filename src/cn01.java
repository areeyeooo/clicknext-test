import java.util.Scanner;
public class cn01 {

	public static void main(String[] args) {
		Scanner kb = new Scanner(System.in);
		//input array
		System.out.print("Enter number: ");
		int n = kb.nextInt();
		
		int [] arr = new int[n];
		//generate array 1 to n
//		for(int i = 0; i < n;i++) {
//			arr[i] = i+1;
//		}
		
		//generate array by input Elements
		System.out.print("Enter element to array: ");
		for(int i = 0; i < n;i++) {
			arr[i] = kb.nextInt();
		}
		//input sum
		System.out.print("Enter sum: ");
		int sum = kb.nextInt();
		
		System.out.print("Result: ");
		for(int i = 0; i < n;i++) {
			for(int j = i; j < n; j++) {
				if(arr[i] + arr[j] == sum) {
					System.out.print("[" + arr[i] + ", " + arr[j] + "]" + " ");
				}
			}
		}
		

	}

}
