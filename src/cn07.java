import java.util.Scanner;

public class cn07 {

	public static void main(String[] args) {
		Scanner kb = new Scanner(System.in);
		System.out.print("input: ");
		int n = kb.nextInt();
		int h = 0, s = 0, m = 0;
		String time = "";

//		System.out.println(n % 60);
//		System.out.println(n / 60);
//		System.out.println((n / 60) % 60);

		if (n >= 60 && n / 60 >= 60) {
			h = (n / 60) / 60;
			m = (n / 60) % 60;
		}
		if (n >= 60 && n / 60 < 60) {
			m = (n / 60);
		}
		if (n % 60 != 0) {
			s = n % 60;
		}

		if (h < 10) {
			time += "0" + h + ":";
		} else {
			time += h + ":";
		}
		if (m < 10) {
			time += "0" + m + ":";
		} else {
			time += m + ":";
		}
		if (s < 10) {
			time += "0" + s;
		} else {
			time += s;
		}

		System.out.println(time);
	}

}
