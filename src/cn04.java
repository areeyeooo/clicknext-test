import java.util.Scanner;
public class cn04 {
	public static void main(String[] args) {
		Scanner kb = new Scanner(System.in);
		System.out.print("Input number: ");
		int num = kb.nextInt();
		for(int i = 1;i<=num;i++) {
			for(int j = 1;j<=num;j++) {
				if(j<i) {
					System.out.print(" ");
				}
				else {
					System.out.print("*");
				}
			}
		    for(int j = num-1;j>=1;j--) {
		    	if(j>=i) {
		    		System.out.print("*");
		    	}
		    	else {
		    		System.out.print(" ");
		    	}
			}
			System.out.println();
		}
	}
}
