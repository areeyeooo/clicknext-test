import java.util.Arrays;
import java.util.Scanner;
public class cn02 {
	public static void main(String[] args) {
		Scanner kb = new Scanner(System.in);
		int count;
		System.out.print("s1: ");
		String s1 = kb.next();
		System.out.print("s2: ");
		String s2 = kb.next();
		
		s1 = s1.toUpperCase();
		s2 = s2.toUpperCase();
		
		//sort1
		char [] arr1 = s1.toCharArray();
		char [] arr1_sorted = new char [s1.length()];
		for(int i = 0;i < arr1.length;i++) {
			count = 0;
			for(int j = 0; j < arr1.length; j++) {
				if(arr1[i] < arr1[j]) {
					count++;
				}
			}
			arr1_sorted[count] = arr1[i];
		}
		
		//sort2
		char [] arr2 = s2.toCharArray();
		char [] arr2_sorted = new char [s2.length()];
		for(int i = 0;i < arr2.length;i++) {
			count = 0;
			for(int j = 0; j < arr2.length; j++) {
				if(arr2[i] < arr2[j]) {
					count++;
				}
			}
			arr2_sorted[count] = arr2[i];
		}
		
		boolean ans = Arrays.equals(arr1_sorted, arr2_sorted);
		System.out.println(ans);
	}

}
