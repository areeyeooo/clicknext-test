import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class cn06 {
	public static void main(String[] args) {
		Scanner kb = new Scanner(System.in);
		System.out.print("Enter number: ");
		int n = kb.nextInt();

		int[] arr = new int[n];
		System.out.print("Enter element to array: ");
		for (int i = 0; i < n; i++) {
			arr[i] = kb.nextInt();
		}
		
		sort(arr);

		for (int i = 0; i<arr.length;i++) {
			System.out.print(arr[i] + " ");
		}
	}
	
	public static void sort(int[] arr) {
		int index = -1;
		
		for (int i = 0; i<arr.length;i++) {
			index = i;
			for (int j = i; j <= arr.length-1;j++) {
				if (arr[j] > arr[index]) {
					index = j;
				}
			}
			int temp = arr[i];
			arr[i] = arr[index];
			arr[index] = temp;
		}
	}
}
