import java.util.Scanner;
import java.util.ArrayList;
public class cn03 {
	public static void main(String[] args) {
		Scanner kb = new Scanner(System.in);
		System.out.print("Enter number: ");
		int n = kb.nextInt();
		int [] arr = new int[n];
		System.out.print("Enter element to array: ");
		for(int i = 0; i < n;i++) {
			arr[i] = kb.nextInt();
		}
		
		//result collection
		ArrayList<String> result = new ArrayList<String>();
		
		boolean check = false;
		int count = 0;
		
		for(int i = 0; i < n;i++ ) {
			if(i != n-1) {
				if(arr[i]-arr[i+1] != -1) {
					if(check == true) {
						int cal = arr[i] - count;
						result.add(Integer.toString(cal) + "-" + arr[i]);
						check = false;
						count = 0;
					} else {
						result.add(Integer.toString(arr[i]));
					}
				} else {
					count++;
					check = true;
				}
				
			} else {
				if (arr[i]-arr[i-1] != 1) {
					result.add(Integer.toString(arr[i]));
				} else {
					int cal = arr[i]-count;
					result.add(Integer.toString(cal) + "-" + arr[i]);
				}
			}
		}
		System.out.println(result);
	}

}
