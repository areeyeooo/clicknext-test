import java.util.Scanner;

public class cn08 {

	public static void main(String[] args) {
		Scanner kb = new Scanner(System.in);
		int x500 = 0, x100 = 0,x50 = 0,x20 = 0,x10 = 0,x5 = 0,x1 = 0;
		System.out.print("input: ");
		int input = kb.nextInt();
		int change = 1000 - input; 
		System.out.println("Change = " + change);
		
		if(change/500 > 0) {
			x500 = change/500;
			change = change - (500*x500);
		}
		if(change/100 > 0) {
			x100 = change/100;
			change = change - (100*x100);
		}
		if(change/50 > 0) {
			x50 = change/50;
			change = change - (50*x50);
		}
		if(change/20 > 0) {
			x20 = change/20;
			change = change - (20*x20);
		}
		if(change/10 > 0) {
			x10 = change/10;
			change = change - (10*x10);
		}
		if(change/5 > 0) {
			x5 = change/5;
			change = change - (5*x5);
		}
		if(change/1 > 0) {
			x1 = change/1;
			change = change - (1*x1);
		}
		
		System.out.println("500 = " + x500 );
		System.out.println("100 = " + x100);
		System.out.println("50 = " + x50);
		System.out.println("20 = " + x20);
		System.out.println("10 = " + x10);
		System.out.println("5 = " + x5);
		System.out.println("1 " + x1);
	}

}
